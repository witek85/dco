<?php get_template_part('templates/page', 'header'); ?>


<div class="wrapper">
  <div class="row row-md-spacing nomargin">

    <div class="col-md-12 nopadding content">

      <div class="alert alert-warning">
        <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
      </div>

      <?php get_search_form(); ?>

    </div>
  </div>
</div>

