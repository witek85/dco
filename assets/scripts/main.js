/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function ($) {
  
//    var $window = $(window),
//    $body = $('body'),
//    $htmlBody = $('html, body'),
    


  function initGoogleMaps() {

    var address = $('#map-canvas').data('address');
    var addressBlocks = address.split(',');

    var address = $('#map-canvas').data('address');
    var addressBlocks = address.split(',');

    var myLng = parseFloat(addressBlocks[addressBlocks.length - 1]);
    var myLat = parseFloat(addressBlocks[addressBlocks.length - 2]);

    console.log('lat ' + myLat);
    console.log('lng ' + myLng);
//    console.log(myLatlng);

    var myLatlng = new google.maps.LatLng(myLat, myLng);
//          var myLatlng = new google.maps.LatLng(52.406374, 16.925168100000064);

    var map = new google.maps.Map(document.getElementById("map-canvas"),
            {
              center: myLatlng,
              zoom: 12,
              scrollwheel: false,
              draggable: false,
              mapTypeId: 'roadmap',
            });

    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Jesteśmy tutaj!'

    });

    google.maps.event.addListener(marker, 'click', function () {
      infowindow.open(map, marker);
    });
  }
  
  var partners_autoplay = $('.partners-slider').data('autoplay');

  $('.partners-slider').slick({
    slidesToShow: 4,
    autoplay: partners_autoplay,
    autoplaySpeed: 3000,
    speed: 200,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1
        }
      },
    ]

  });
  
//  $('.lazy').lazyload({
//      effect : "fadeIn"
//  });
  
   var bLazy = new Blazy({
        // Options
    });
	

  var autoplay = $('.main-slider').data('autoplay');

  $('.main-slider').slick({
    slidesToShow: 1,
    autoplay: autoplay,
    autoplaySpeed: 5000,
    dots: true,
    infinite: true,
    speed: 200
  });

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function () {
        // JavaScript to be fired on all pages

      },
      finalize: function () {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function () {
        // JavaScript to be fired on the home page
      },
      finalize: function () {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'kontakt': {
      init: function () {
        initGoogleMaps();
      }
    },
    'redmine': {
      init: function () {
        var password = $('.page-template-template-locked').find('.password');
        var redirect = password.data('redirect');

        if (password.hasClass('password-ok')) {
          window.location = redirect;
//          var win = window.open(redirect, '_blank');
//          win.focus();
        }
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function (func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function () {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
