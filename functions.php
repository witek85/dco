<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/utils.php',                 // Utility functions
  'lib/init.php',                  // Initial theme setup and constants
  'lib/wrapper.php',               // Theme wrapper class
  'lib/conditional-tag-check.php', // ConditionalTagCheck class
  'lib/config.php',                // Configuration
  'lib/assets.php',                // Scripts and stylesheets
  'lib/titles.php',                // Page titles
  'lib/extras.php',                // Custom functions
  'lib/wp_bootstrap_navwalker.php',
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

function my_custom_post_partnerzy() {
  $labels = array(
    'name'               => _x( 'Partnerzy', 'post type general name' ),
    'singular_name'      => _x( 'Partner', 'post type singular name' ),
    'add_new'            => _x( 'Dodaj nowy', 'book' ),
    'add_new_item'       => __( 'Dodaj nowego partnera' ),
    'edit_item'          => __( 'Edytuj partnera' ),
    'new_item'           => __( 'Nowy partner' ),
    'all_items'          => __( 'Wszyscy' ),
    'view_item'          => __( 'Zobacz partnera' ),
    'search_items'       => __( 'Przeszukaj partnerów' ),
    'not_found'          => __( 'Nie znaleziono' ),
    'not_found_in_trash' => __( 'Nie znaleziono w koszu' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Partnerzy'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Partnerzy',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'thumbnail' ),
    'has_archive'   => false,
  );
  register_post_type( 'partnerzy', $args ); 
}
add_action( 'init', 'my_custom_post_partnerzy' );

function my_custom_post_slider() {
  $labels = array(
    'name'               => _x( 'Slider', 'post type general name' ),
    'singular_name'      => _x( 'Slider', 'post type singular name' ),
    'add_new'            => _x( 'Dodaj nowy', 'book' ),
    'add_new_item'       => __( 'Dodaj nowy slajd' ),
    'edit_item'          => __( 'Edytuj slajdy' ),
    'new_item'           => __( 'Nowy slajd' ),
    'all_items'          => __( 'Wszystkie' ),
    'view_item'          => __( 'Zobacz slajd' ),
    'search_items'       => __( 'Przeszukaj slajdy' ),
    'not_found'          => __( 'Nie znaleziono' ),
    'not_found_in_trash' => __( 'Nie znaleziono w koszu' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Slider'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Główny slider',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor', 'thumbnail' ),
    'has_archive'   => false,
  );
  register_post_type( 'slider', $args ); 
}
add_action( 'init', 'my_custom_post_slider' );

apply_filters ( 'post_password_expires', 1 );

function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyC07Mza4uIdBtaWgaTZSUSUiZIQAYqx0D8';
	
	return $api;
	
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
