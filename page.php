<?php // while (have_posts()) : the_post(); ?>
  <?php // get_template_part('templates/page', 'header'); ?>
<?php // endwhile; ?>

<?php get_template_part('templates/page', 'header'); ?>

  <div class="wrapper">
    <div class="row row-md-spacing nomargin">
      
      <div class="col-md-12 nopadding content">

      <?php while (have_posts()) : the_post(); ?>
          
        <?php get_template_part('templates/content', 'page'); ?>

      <?php endwhile; ?>
      </div>      
    </div>
  </div>