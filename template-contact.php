<?php
/**
 * Template Name: Kontakt
 */
?>
<div id="map-canvas" data-address="<?= the_field('mapa'); ?>"></div>

<div class="wrapper">
  <div class="row row-md-spacing nomargin">
    <div class="col-md-3 nopadding sidebar">
      <?php dynamic_sidebar('sidebar-contact'); ?>
    </div>
    <div class="col-md-9 nopadding content">

      <?php while (have_posts()) : the_post(); ?>

        <?php get_template_part('templates/content', 'page'); ?>

      <?php endwhile; ?>
    </div>


  </div>
</div>