<?php
/**
 * Template Name: Strona główna
 */
?>

<?php get_template_part('templates/homepage', 'slider'); ?>

  <div class="wrapper homepage-wrapper">
    <div class="row row-md-spacing content">
      
      <!--<div class="col-md-12 nopadding nomargin ">-->

      <?php while (have_posts()) : the_post(); ?>

        <?php get_template_part('templates/content', 'page'); ?>

      <?php endwhile; ?>
      <!--</div>-->
        
    </div>
  </div>