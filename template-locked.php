<?php
/**
 * Template Name: Hasło
 */
?>

<?php get_template_part('templates/page', 'header'); ?>

  <div class="wrapper password <?php if ( post_password_required() ) : ?>password-locked<?php else: ?>password-ok<?php endif; ?>" data-redirect="<?php the_field('redirect'); ?>">
    <div class="row row-md-spacing nomargin">
      
      <div class="col-md-12 content">

      <?php while (have_posts()) : the_post(); ?>
          
        <?php get_template_part('templates/content', 'page'); ?>

      <?php endwhile; ?>
      </div>
        
    </div>
  </div>