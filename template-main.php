<?php
/**
 * Template Name: Main Landing Page
 */
?>

<div class="wrapper">
  <div class="row row-md-spacing nomargin content">
    <div class="col-md-12 nopadding content">
      <div class="image-row">
        <div class="image-box">
          <a href="/" title="Strona główna" class="logo-holder">
            <div class="logo2"></div>
          </a>
          <img class="alignnone wp-image-9 size-full" src="http://dco.org.pl/wp-content/uploads/2016/08/home_landing5_03.png" alt="home_landing5_03" width="430" height="624" />
          <a class="button button-clients" href="http://partnerzy.dco.org.pl/">Strefa centrum ogrodniczego</a>
          <a class="button button-partners" href="http://klienci.dco.org.pl/">Strefa klienta detalicznego</a>
        </div>
      </div>
      <?php while (have_posts()) : the_post(); ?>

        <?php get_template_part('templates/content', 'page'); ?>

      <?php endwhile; ?>
    </div>

  </div>
</div>