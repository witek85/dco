<footer class="footer wrapper" role="contentinfo">
  <div class="footer-separator"><span>Nasza grupa zakupowa</span></div>
  <div class="footer-box">
    <!--<div class="row nomargin">-->

      <?php //dynamic_sidebar('sidebar-footer'); ?>

      <div class="partners-slider" data-autoplay=<?= the_field('partnerzy_autoplay'); ?>>
        <?php
        $temp = $wp_query;
        $wp_query = null;
        $wp_query = new WP_Query();
        $wp_query->query('post_type=partnerzy&posts_per_page=-1');

        while ($wp_query->have_posts()) : $wp_query->the_post();
          ?>

        <div class="slide">

            <?php if (get_field('partnerzy_link')): ?>
              <div class="slide-img">
                <a href="<?= the_field('partnerzy_link'); ?>" title="<?php the_title(); ?>" target="_blank">
                  <?php the_post_thumbnail('partners'); ?>
                </a>
              </div>
            <?php else: ?>
              <div class="slide-img">
                <?php the_post_thumbnail('partners'); ?>
              </div>
            <?php endif; ?>
            
          </div>
        <?php endwhile; ?>
        <?php
        $wp_query = null;
        $wp_query = $temp;
        ?>
        <?php wp_reset_query(); ?>

      </div>
    <!--</div>-->
  </div>
  <div class="row copyright nomargin">
    <div class="col-md-12 nopadding text-center">
      <p>Copyright <sup>©</sup>2016&nbsp;<a href="/" title="Strona główna">DCO.ORG.PL</a>&nbsp;|&nbsp;Wykonanie:&nbsp;<a href="http://www.adimo.pl/" target="_blank" title="Adimo.pl - Technologie internetowe">ADIMO.PL</a></p>
    </div>
  </div>
</footer>