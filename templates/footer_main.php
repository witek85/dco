<footer class="footer wrapper" role="contentinfo">

  <div class="row copyright nomargin">
    <div class="col-md-6 nopadding text-left">
      <p>Copyright <sup>©</sup>2016&nbsp;<a href="/" title="Strona główna">DCO.ORG.PL</a></p>
    </div>
    <div class="col-md-6 nopadding text-right">
      <p>Wykonanie:&nbsp;<a href="http://www.adimo.pl/" target="_blank" title="Adimo.pl - Technologie internetowe">ADIMO.PL</a></p>
    </div>
  </div>
</footer>