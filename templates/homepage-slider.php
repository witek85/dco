<div class="main-slider" data-autoplay=<?= the_field('slider_autoplay'); ?> >
  <?php
  $temp = $wp_query;
  $wp_query = null;
  $wp_query = new WP_Query();
  $wp_query->query('post_type=slider');

  while ($wp_query->have_posts()) : $wp_query->the_post();
    ?>

    <div class="slide">
      <div class="slide-background" style="background-image: url('<?php the_post_thumbnail_url('main-slider'); ?>')">
         <div class="slide-header">
        <h2><?php the_title(); ?></h2>
        <span><?php the_content(); ?></span>
      </div>
      </div>
      <?php // the_post_thumbnail('main-slider'); ?>
     
    </div>

  <?php endwhile; ?>
  <?php
  $wp_query = null;
  $wp_query = $temp;
  ?>
  <?php wp_reset_query(); ?>

</div>