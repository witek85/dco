<?php use Roots\Sage\Titles; ?>

  <?php if( get_field('tlo-naglowka') ): ?>
  <header class="header b-lazy" data-src="<?php the_field('tlo-naglowka'); ?>">
  <?php else: ?>
  <header class="header b-lazy" data-src="/wp-content/themes/dco/dist/images/header.jpg">
  <?php endif; ?>
    
  <div class="wrapper">
    <div class="header-container">
      <h3><?= Titles\title(); ?></h3>
      <p><?= the_field('podtytul'); ?></p>
    </div>

  </div>
</header>